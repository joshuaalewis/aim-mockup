export const styles = theme => ({
    root: {
        // backgroundColor: theme.palette.primary.main,
        background: `linear-gradient(to bottom right, ${theme.palette.primary[600]}, ${theme.palette.primary[300]})`,
        width: '100%',
        height: '100%',
        textAlign: 'center'
    },
    wrapper: {
        margin: 'auto',
        width: '30%',
        paddingTop: 150,
        [theme.breakpoints.down("sm")]: {
            width: '90%',
        }
    },
    header: {
        fontSize: 34,
        color: theme.palette.background.paper,
        padding: '0 0 15px 0',
    },
    logo: {
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center 20%',
        margin: '0 auto',
        height: '7vmax',
        minHeight: '80px'
    },
    footer: {
        fontSize: 14,
        fontWeight: 500,
        color: theme.palette.background.paper,
        padding: 20
    }
});