export const styles = theme => ({
    root: {
        padding: '20px 10px',
        textAlign: 'center',
        margin: '0 30px',
    },
    iconSection: {
        textAlign: 'center',
    },
    icon: {
        fontSize: 80,
        color: theme.palette.primary.main,
    },
    formSection: {
        margin: '10px 35px 90px 35px'
    },
    textField: {
        marginBottom: 18,
        width: '100%',
    },
    buttonSection: {
        margin: '15 auto'
    },
    loginButton: {
        borderRadius: 30,
        padding: "20px 40px",
        color: 'white'
    }
});