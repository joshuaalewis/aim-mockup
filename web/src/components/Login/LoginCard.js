import React, { Component } from 'react';
import withStyles from "@material-ui/core/es/styles/withStyles";
import {styles} from "./LoginCard.styles.js";
import Card from "@material-ui/core/es/Card/Card";
import Button from "@material-ui/core/es/Button/Button";
import {AccountCircle, Facebook, Twitter} from "mdi-material-ui";
import Paper from "@material-ui/core/es/Paper/Paper";
import TextField from "@material-ui/core/es/TextField/TextField";

class LoginCard extends Component {
    state = {
        username: "",
        password: "",
        loginAttempt: false,
    };

    handleFormChange = field => event => {
        this.setState({ [field]: event.target.value });
    };

    handleClick = () => {
        this.setState({loginAttempt: true});

        if(this.state.username && this.state.password) {
            this.props.login(this.state.username, this.state.password);
        }
    };

    handleKeyPress = event => {
        if (event.key === 'Enter') {
            this.props.login(this.state.username, this.state.password);
        }
    };

    clearErrors = () => {
        this.setState({loginAttempt: false});
    };

    render() {
        let { classes, theme, children, login } = this.props;

        return (
            <Card className={classes.root} onKeyPress={this.handleKeyPress}>
                <div className={classes.iconSection}>
                    <AccountCircle className={classes.icon}/>
                </div>
                <div className={classes.formSection}>
                    <TextField id={"username"}
                               label={"Name"}
                               value={this.state.username}
                               onChange={this.handleFormChange('username')}
                               className={classes.textField}
                               error={this.state.loginAttempt && !this.state.username}
                               onClick={this.clearErrors}
                    />
                    <TextField id={"password"}
                               label={"Password"}
                               value={this.state.password}
                               onChange={this.handleFormChange('password')}
                               type={"password"}
                               className={classes.textField}
                               color={"secondary"}
                               error={this.state.loginAttempt && !this.state.password}
                               onClick={this.clearErrors}
                    />
                </div>
                <div className={classes.buttonSection}>
                    <Button variant={"raised"}
                            color={"primary"}
                            classes={{root:classes.loginButton}}
                            onClick={this.handleClick}
                    >
                        Login
                    </Button>
                </div>
            </Card>

        )
    }

}

export default withStyles(styles, {withTheme: true})(LoginCard);