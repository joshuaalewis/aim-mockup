import React, { Component } from 'react';
import withStyles from "@material-ui/core/es/styles/withStyles";

import {styles} from "./LoginLayout.styles.js";

class LoginLayout extends Component {

    render() {
        let { classes, theme, children } = this.props;
        let logoUrl = require('../../assets/namedLogo.png');
        console.log(theme);

        return (
            <div className={classes.root}>
                <div className={classes.wrapper}>
                    {children}
                    <div className={classes.footer}>
                        Don't have an account? Sign up!
                    </div>
                </div>
            </div>

        )
    }

}

export default withStyles(styles, {withTheme: true})(LoginLayout);