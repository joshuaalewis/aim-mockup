import React, { Component } from 'react';
import withStyles from "@material-ui/core/es/styles/withStyles";

import {styles} from "./SideDrawer.styles.js";
import Drawer from "@material-ui/core/es/Drawer/Drawer";
import {
    Database, Finance,
    Gauge,
    GaugeEmpty,
    TableEdit,
    ViewDashboardVariant,
    Widgets
} from "mdi-material-ui";
import Divider from "@material-ui/core/es/Divider/Divider";
import List from "@material-ui/core/es/List/List";
import ListItem from "@material-ui/core/es/ListItem/ListItem";
import ListItemIcon from "@material-ui/core/es/ListItemIcon/ListItemIcon";
import ListItemText from "@material-ui/core/es/ListItemText/ListItemText";
import Avatar from "@material-ui/core/es/Avatar/Avatar";
import {Routes} from "../utilities/Enums";

class SideDrawer extends Component {

    navigate = (path) => {
        this.props.history.push(path);
    };

    render() {
        let { classes, theme, children, history, toggleDrawer, drawerOpen } = this.props;

        return (
            <Drawer
                classes={{paper: classes.drawer}}
                open={drawerOpen}
                onClick={toggleDrawer}
                history={history}>
                <div className={classes.iconSection}>
                    <Avatar className={classes.avatar}>
                        <Finance className={classes.mainIcon}/>
                    </Avatar>
                </div>
                <Divider />
                <List className={classes.list}>
                    <ListItem button onClick={() => this.navigate(Routes.DASHBOARD.path)}>
                        <ListItemIcon>
                            <Gauge />
                        </ListItemIcon>
                        <ListItemText primary="Dashboard 1" />
                    </ListItem>
                    <ListItem button onClick={() => this.navigate(Routes.DASHBOARD.path)}>
                        <ListItemIcon>
                            <GaugeEmpty />
                        </ListItemIcon>
                        <ListItemText primary="Dashboard 2" />
                    </ListItem>
                    <ListItem button onClick={() => this.navigate(Routes.DASHBOARD.path)}>
                        <ListItemIcon>
                            <ViewDashboardVariant />
                        </ListItemIcon>
                        <ListItemText primary="Dashboard 3" />
                    </ListItem>
                </List>
                <Divider />
                <List>
                    <ListItem button onClick={() => this.navigate(Routes.WIDGETS.path)}>
                        <ListItemIcon>
                            <Widgets />
                        </ListItemIcon>
                        <ListItemText primary="Widgets" />
                    </ListItem>
                    <ListItem button onClick={() => this.navigate(Routes.DATA.path)}>
                        <ListItemIcon>
                            <TableEdit />
                        </ListItemIcon>
                        <ListItemText primary="Data" />
                    </ListItem>
                    <ListItem button onClick={() => this.navigate(Routes.SOURCES.path)}>
                        <ListItemIcon>
                            <Database />
                        </ListItemIcon>
                        <ListItemText primary="Sources" />
                    </ListItem>
                </List>
                <Divider />
            </Drawer>
        )
    }

}

export default withStyles(styles, {withTheme: true})(SideDrawer);