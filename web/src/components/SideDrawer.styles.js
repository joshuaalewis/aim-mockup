export const styles = theme => ({
    drawer: {
        width: 250,
        height: '100%',
    },
    iconSection: {
        textAlign: 'center',
        margin: '40px auto'
    },
    avatar: {
        height: 90,
        width: 90,
        backgroundColor: theme.palette.secondary.main
    },
    mainIcon: {
        height: 60,
        width: 60
    },
    list: {
        paddingTop: 10,
    }
});