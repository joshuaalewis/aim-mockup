export const styles = theme => ({
    card: {

    },
    cardContent: {
        transition: [["all", "500ms"]],
        '&.collapsed': {
            height: 0,
            padding: 0,
            transition: [["all", "500ms"]]
        }
    },
    table: {
        transition: [["all", "500ms"]],
        '&.collapsed': {
            height: 0,
            opacity: 0,
            padding: 0,
            transition: [["all", "500ms"]]
        }
    },
    textField: {
        fontSize: 12,
        width: '90%'
    },
    tableCell: {
        paddingLeft: 2,
        paddingRight: 2
    },
    cardHeader: {

    }
});