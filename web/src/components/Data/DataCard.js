import React, { Component } from 'react';
import withStyles from "@material-ui/core/es/styles/withStyles";

import {styles} from "./DataCard.styles.js";
import IconButton from "@material-ui/core/es/IconButton/IconButton";
import {
    AccountCircle,
    ChartDonutVariant,
    CheckboxMarkedCircleOutline,
    ChevronDown,
    ChevronUp,
    CircleEditOutline,
    Database,
    Delete,
    DotsVertical,
    Flash,
    Forum,
    Heart,
    Menu,
    ShareVariant,
    TableSearch,
} from "mdi-material-ui";
import CardHeader from "@material-ui/core/es/CardHeader/CardHeader";
import Card from "@material-ui/core/es/Card/Card";
import Avatar from "@material-ui/core/es/Avatar/Avatar";
import CardContent from "@material-ui/core/es/CardContent/CardContent";
import CardActions from "@material-ui/core/es/CardActions/CardActions";
import Table from "@material-ui/core/es/Table/Table";
import TableHead from "@material-ui/core/es/TableHead/TableHead";
import TableRow from "@material-ui/core/es/TableRow/TableRow";
import TableCell from "@material-ui/core/es/TableCell/TableCell";
import TableBody from "@material-ui/core/es/TableBody/TableBody";
import TextField from "@material-ui/core/es/TextField/TextField";

class DataCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            expanded: true,
            data: props.data,
            rowEditing: null,
        }
    }

    handleDelete = (index) => {
        let data = [ ...this.state.data ];
        data.splice(index, 1);
        this.setState({data: data});
    };

    handleChange = (prop, index) => event => {
        let data = [ ...this.state.data ];
        data[index][prop] = event.target.value;
        this.setState({data: data});
    };

    render() {
        let { classes, theme, children, title, subtitle } = this.props;
        let { expanded, data, rowEditing } = this.state;
        let collapsed = !expanded ? "collapsed" : "";

        return (
            <Card className={classes.card}>
                <CardHeader
                    avatar={
                        <Avatar aria-label="Recipe" style={{backgroundColor: theme.palette.secondary.main}}>
                            <TableSearch />
                        </Avatar>
                    }
                    action={
                        <IconButton onClick={() => this.setState({expanded: !this.state.expanded})}>
                            {expanded && <ChevronDown />}
                            {!expanded && <ChevronUp />}
                        </IconButton>
                    }
                    title={title}
                    subheader={subtitle}
                    className={`${classes.cardHeader} ${collapsed}`}
                />
                <CardContent classes={{root: `${classes.cardContent} ${collapsed}`}}>
                    <Table className={`${classes.table} ${collapsed}`}>
                        <TableHead>
                            <TableRow>
                                <TableCell>OEM</TableCell>
                                <TableCell>Model</TableCell>
                                <TableCell>Request</TableCell>
                                <TableCell>Date</TableCell>
                                <TableCell>Time</TableCell>
                                <TableCell>Region</TableCell>
                                <TableCell>Amount</TableCell>
                                <TableCell>Diff</TableCell>
                                <TableCell numeric>Actions</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {data.map((n, key) => {
                                return (
                                    <TableRow key={key}>
                                        <TableCell>
                                            {(rowEditing !== key) && n.OEM}
                                            {(rowEditing === key) &&
                                            <TextField
                                                id={key}
                                                className={classes.textField}
                                                InputProps={{
                                                    classes: {
                                                        input: classes.textField,
                                                    }
                                                }}
                                                value={n.OEM}
                                                onChange={this.handleChange('OEM', key)}
                                                margin="normal"
                                            />}
                                        </TableCell>
                                        <TableCell>
                                            {(rowEditing !== key) && n.Model}
                                            {(rowEditing === key) &&
                                            <TextField
                                                id={key}
                                                className={classes.textField}
                                                InputProps={{
                                                    classes: {
                                                        input: classes.textField,
                                                    }
                                                }}
                                                value={n.Model}
                                                onChange={this.handleChange('Model', key)}
                                                margin="normal"
                                            />}
                                        </TableCell>
                                        <TableCell>
                                            {(rowEditing !== key) && n.Request}
                                            {(rowEditing === key) &&
                                            <TextField
                                                id={key}
                                                className={classes.textField}
                                                InputProps={{
                                                    classes: {
                                                        input: classes.textField,
                                                    }
                                                }}
                                                value={n.Request}
                                                onChange={this.handleChange('Request', key)}
                                                margin="normal"
                                            />}
                                        </TableCell>
                                        <TableCell>
                                            {(rowEditing !== key) && n.Date}
                                            {(rowEditing === key) &&
                                            <TextField
                                                id={key}
                                                className={classes.textField}
                                                InputProps={{
                                                    classes: {
                                                        input: classes.textField,
                                                    }
                                                }}
                                                value={n.Date}
                                                onChange={this.handleChange('Date', key)}
                                                margin="normal"
                                            />}
                                        </TableCell>
                                        <TableCell>
                                            {(rowEditing !== key) && n.Time}
                                            {(rowEditing === key) &&
                                            <TextField
                                                id={key}
                                                className={classes.textField}
                                                InputProps={{
                                                    classes: {
                                                        input: classes.textField,
                                                    }
                                                }}
                                                value={n.Time}
                                                onChange={this.handleChange('Time', key)}
                                                margin="normal"
                                            />}
                                        </TableCell>
                                        <TableCell>
                                            {(rowEditing !== key) && n.Region}
                                            {(rowEditing === key) &&
                                            <TextField
                                                id={key}
                                                className={classes.textField}
                                                InputProps={{
                                                    classes: {
                                                        input: classes.textField,
                                                    }
                                                }}
                                                value={n.Region}
                                                onChange={this.handleChange('Region', key)}
                                                margin="normal"
                                            />}
                                        </TableCell>
                                        <TableCell>
                                            {(rowEditing !== key) && n.Static_Amount}
                                            {(rowEditing === key) &&
                                            <TextField
                                                id={key}
                                                className={classes.textField}
                                                InputProps={{
                                                    classes: {
                                                        input: classes.textField,
                                                    }
                                                }}
                                                value={n.Static_Amount}
                                                onChange={this.handleChange('Static_Amount', key)}
                                                margin="normal"
                                            />}
                                        </TableCell>
                                        <TableCell>
                                            {(rowEditing !== key) && n.Diff}
                                            {(rowEditing === key) &&
                                            <TextField
                                                id={key}
                                                className={classes.textField}
                                                InputProps={{
                                                    classes: {
                                                        input: classes.textField,
                                                    }
                                                }}
                                                value={n.Diff}
                                                onChange={this.handleChange('Diff', key)}
                                                margin="normal"
                                            />}
                                        </TableCell>
                                        {(rowEditing !== key) &&
                                        <TableCell numeric style={{paddingRight: 5, width: 100}}>
                                            <IconButton onClick={() => this.setState({rowEditing: key})}>
                                                <CircleEditOutline style={{color: theme.palette.secondary.main, padding: 5}}/>
                                            </IconButton>
                                            <IconButton onClick={() => this.handleDelete(key)}>
                                                <Delete style={{color: theme.palette.secondary.main, padding: 5}}/>
                                            </IconButton>
                                        </TableCell>}
                                        {(rowEditing === key) &&
                                        <TableCell numeric style={{paddingRight: 5, width: 100}}>
                                            <IconButton onClick={() => this.setState({rowEditing: null})}>
                                                <CheckboxMarkedCircleOutline style={{color: theme.palette.secondary.main, padding: 5}}/>
                                            </IconButton>
                                        </TableCell>}
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </CardContent>
                <CardActions style={{display: 'flex'}} disableActionSpacing>
                    <IconButton aria-label="Add to favorites">
                        <ShareVariant />
                    </IconButton>
                    <IconButton aria-label="Share">
                        <Database />
                    </IconButton>
                </CardActions>
            </Card>
        )
    }

}

export default withStyles(styles, {withTheme: true})(DataCard);