import React, { Component } from 'react';
import withStyles from "@material-ui/core/es/styles/withStyles";

import {styles} from "./MainLayout.styles.js";
import AppBar from "@material-ui/core/es/AppBar/AppBar";
import Toolbar from "@material-ui/core/es/Toolbar/Toolbar";
import IconButton from "@material-ui/core/es/IconButton/IconButton";
import {
    AccountCircle,
    Flash,
    Forum,
    Menu,
} from "mdi-material-ui";
import SideDrawer from "./SideDrawer";

class MainLayout extends Component {
    state = {
        drawerOpen: false,
        menuOpen: false,
        anchorEl: null,
    };

    toggleDrawer = () => {
        this.setState({drawerOpen: !this.state.drawerOpen});
    };

    render() {
        let { classes, theme, children, logout, history } = this.props;
        let { drawerOpen, anchorEl } = this.state;

        return (
            <div className={classes.root}>
                <SideDrawer toggleDrawer={this.toggleDrawer} drawerOpen={drawerOpen} history={history}/>
                <AppBar elevation={5} position={"static"} classes={{root: classes.appBar}}>
                    <Toolbar >
                        <IconButton
                            onClick={this.toggleDrawer}>
                            <Menu style={{color: 'white'}}/>
                        </IconButton>
                        <div style={{flex: 1}}>
                            <h2 style={{fontWeight: 400, marginLeft: 30}}>
                                AIM
                            </h2>
                        </div>
                        <IconButton>
                            <Flash style={{color: 'white'}}/>
                        </IconButton>
                        <IconButton>
                            <Forum style={{color: 'white'}}/>
                        </IconButton>
                        <IconButton
                            onClick={logout}
                            aria-owns={anchorEl ? 'simple-menu' : null}
                            aria-haspopup="true">
                            <AccountCircle style={{color: 'white'}}/>
                        </IconButton>
                    </Toolbar>
                </AppBar>
                <div className={classes.childContainer}>
                    {children}
                </div>
            </div>

        )
    }

}

export default withStyles(styles, {withTheme: true})(MainLayout);