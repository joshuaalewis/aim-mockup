import React, { Component } from 'react';
import withStyles from "@material-ui/core/es/styles/withStyles";

import {styles} from "./WidgetCard.styles.js";
import IconButton from "@material-ui/core/es/IconButton/IconButton";
import {
    AccountCircle, ChartDonutVariant, DotsVertical,
    Flash,
    Forum, Heart,
    Menu, ShareVariant,
} from "mdi-material-ui";
import CardHeader from "@material-ui/core/es/CardHeader/CardHeader";
import Card from "@material-ui/core/es/Card/Card";
import Avatar from "@material-ui/core/es/Avatar/Avatar";
import CardContent from "@material-ui/core/es/CardContent/CardContent";
import CardActions from "@material-ui/core/es/CardActions/CardActions";
import Amcharts from "@amcharts/amcharts3-react";

class WidgetCard extends Component {

    componentDidMount() {
        this.clearLogo();
    }

    clearLogo() {
        setTimeout(() => {
            let matches = document.querySelectorAll("a");

            matches.forEach(match => {
                match.parentNode.removeChild(match);
            });

        }, 500);

    }

    render() {
        let { classes, theme, children, title, subtitle, data, largeDisplay, chartOptions } = this.props;

        return (
            <Card >
                <CardHeader
                    avatar={
                        <Avatar aria-label="Recipe" style={{backgroundColor: theme.palette.secondary.main}}>
                            <ChartDonutVariant />
                        </Avatar>
                    }
                    action={
                        <IconButton>
                            <DotsVertical />
                        </IconButton>
                    }
                    title={title}
                    subheader={subtitle}
                />
                <CardContent classes={{root: classes.cardContent}}>
                    <Amcharts.React
                        style={{
                            width: "100%",
                            height: "220px",
                        }}
                        options={{
                            ...chartOptions,
                            colors: [theme.palette.error.main, theme.palette.primary[500], theme.palette.secondary[500], theme.palette.primary[800], theme.palette.secondary[800], theme.palette.primary[200]],
                            dataProvider: data,
                        }}
                    />
                </CardContent>
                <CardActions style={{display: 'flex'}} disableActionSpacing>
                    <IconButton aria-label="Add to favorites">
                        <ShareVariant />
                    </IconButton>
                    <IconButton aria-label="Share">
                        <Heart />
                    </IconButton>
                </CardActions>

            </Card>
        )
    }

}

export default withStyles(styles, {withTheme: true})(WidgetCard);