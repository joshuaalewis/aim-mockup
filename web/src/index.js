// external
import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import {MuiThemeProvider} from "@material-ui/core/styles";
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";

// internal
import App from './containers/App';
import {Store, Theme} from "./utilities";
import './index.css';

ReactDOM.render(
    <MuiThemeProvider theme={Theme}>
        <BrowserRouter>
            <Provider store={Store}>
                <App />
            </Provider>
        </BrowserRouter>
    </MuiThemeProvider>
    , document.getElementById('root'));
registerServiceWorker();
