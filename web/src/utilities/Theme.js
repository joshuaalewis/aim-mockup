import {createMuiTheme} from "@material-ui/core/styles";
import red from "@material-ui/core/es/colors/red";
import teal from "@material-ui/core/es/colors/teal";
import lightBlue from "@material-ui/core/es/colors/lightBlue";

export const Theme = createMuiTheme({
    palette: {
        primary: teal,
        secondary: lightBlue,
        error: red,
    },
});