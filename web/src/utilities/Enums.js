import Login from "../containers/Login";
import Root from "../containers/Root";
import Dashboard from "../containers/Dashboard";
import Widgets from "../containers/Widgets";
import Sources from "../containers/Sources";
import Data from "../containers/Data";

export const Routes = {
    LOGIN: {
        name: "Login",
        path: "/login/",
        container: Login,
    },
    REGISTER: {
        name: "Register",
        path: "/register/",
        container: null,
    },
    ROOT: {
        name: "root",
        path: "/",
        container: Root,
    },
    DASHBOARD: {
        name: "Dashboard",
        path: "/dashboard/",
        container: Dashboard,
    },
    WIDGETS: {
        name: "Widgets",
        path: "/widgets/",
        container: Widgets,
    },
    SOURCES: {
        name: "Sources",
        path: "/sources/",
        container: Sources,
    },
    DATA: {
        name: "Data",
        path: "/data/",
        container: Data,
    },
};