import axios from 'axios';

// TODO handle errors better, check for null response, check for null data, etc.

class HttpService {
    static get(resourceName, queryParams=[], baseURL=(this.basePath)) {
        axios.defaults.baseURL = baseURL;
        axios.defaults.responseType = "application/json";
        axios.defaults.headers.get['Access-Control-Allow-Origin'] = '*';

        return axios.get(`/${resourceName}${this.buildQuery(queryParams)}`, {withCredentials: true})
            .then(response => {
                return response.data;
            })
            .catch(err => {
                if(err.response) {
                    let status = err.response.status;
                    if(status === 401) {
                        //TODO
                        console.log("Unuauthorized");
                    }
                }
            });
    }

    static post(resourceName, object, queryParams=[]) {
        axios.defaults.baseURL = this.basePath;
        axios.defaults.headers.post['Content-Type'] = 'application/json';
        axios.defaults.headers.get['Access-Control-Allow-Origin'] = '*';

        return axios.post(`/${resourceName}${this.buildQuery(queryParams)}`, object, {withCredentials: true})
            .then((response) => {
                return response.data;
            })
            .catch((err) => {
                if(err.response) {
                    let status = err.response.status;
                    if(status === 401) {
                        //TODO
                        console.log("Unuauthorized");
                    }
                }
            });
    }

    static put(resourceName, object, queryParams=[]) {
        axios.defaults.baseURL = this.basePath;
        axios.defaults.headers.post['Content-Type'] = 'application/json';
        axios.defaults.headers.get['Access-Control-Allow-Origin'] = '*';

        return axios.put(`/${resourceName}${this.buildQuery(queryParams)}`, object, {withCredentials: true})
            .then((response) => {
                return response.data;
            })
            .catch((err) => {
                if(err.response) {
                    let status = err.response.status;
                    if(status === 401) {
                        //TODO
                        console.log("Unuauthorized");
                    }
                }
            });
    }

    static delete(resourceName, id) {
        axios.defaults.baseURL = this.basePath;
        axios.defaults.headers.get['Access-Control-Allow-Origin'] = '*';

        return axios.delete(`/${resourceName}/${id}/delete`, {withCredentials: true})
            .then((response) => {
                return response.data;
            })
            .catch((err) => {
                if(err.response) {
                    let status = err.response.status;
                    if(status === 401) {
                        //TODO
                        console.log("Unuauthorized");
                    }
                }
            });
    }

    static buildQuery(queryParams) {
        if (queryParams && queryParams.length > 0) {
            let result = "?";
            for(let i in queryParams) {
                let p = queryParams[i];
                if(i > 0) result = result + "&";
                result = result + `${p.name}=${p.value}`
            }

            return result;
        } else return ""

    }

    static get basePath() {
        return "/api/";
    }
}

export default HttpService;