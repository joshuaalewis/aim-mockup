import React, { Component } from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import {} from "../actions";
import {Routes} from "../utilities/Enums";
import Redirect from "react-router-dom/es/Redirect";
import Card from "@material-ui/core/es/Card/Card";
import Grid from "@material-ui/core/es/Grid/Grid";
import MainLayout from "../components/MainLayout";
import CardHeader from "@material-ui/core/es/CardHeader/CardHeader";
import Avatar from "@material-ui/core/es/Avatar/Avatar";
import IconButton from "@material-ui/core/es/IconButton/IconButton";
import {
    ChartArc,
    ChartBar,
    ChartBubble,
    ChartGantt,
    ChartLine,
    Check,
    DotsVertical,
    Gauge,
    Pencil,
    Sitemap
} from "mdi-material-ui";
import TextField from "@material-ui/core/es/TextField/TextField";

const mockData = [
    {
        title: "Sales Projection",
        selected: 4
    },
    {
        title: "Sales Distribution",
        selected: 1
    },
    {
        title: "Quarterly Earnings",
        selected: 3
    },
    {
        title: "Quarter Report",
        selected: 2
    },
    {
        title: "Salesman Breakdown",
        selected: 0
    },
    {
        title: "Sales Projection",
        selected: 5
    },
];

class Widgets extends Component {
    constructor(props) {
        super(props);
        this.state = {
            editing: null,
            mocks: mockData,
        }
    }

    logout = () => {
        this.props.history.push(Routes.LOGIN.path);
    };

    handleClick = (selected, index) => {
        let mocks = [ ...this.state.mocks ];
        mocks[index].selected = selected;
        this.setState({mocks: mocks});
    };

    render() {
        const styles = {
            wrapper: {
                margin: '30px auto',
                width: '60%'
            },
            grid: {
                margin: 20,
                textAlign: 'center'
            },
            icon: {
                width: 80,
                height: 80,
            },
            card: {
                margin: 15,
            }
        };
        let { history } = this.props;
        let { editing, mocks } = this.state;

        return (
            <MainLayout logout={this.logout} history={history}>
                <div style={styles.wrapper}>
                    {mocks.map((mock, key) => {
                        return (
                            <Card key={key} style={styles.card}>
                                <CardHeader
                                    avatar={
                                        <Avatar aria-label="Source" style={{backgroundColor: '#00b0ff'}}>
                                            <Sitemap />
                                        </Avatar>
                                    }
                                    action={
                                        <IconButton onClick={() => this.handleEdit()}>
                                            {/*{(editing !== key) && <Pencil />}*/}
                                            {/*{(editing === key) && <Check />}*/}
                                            <DotsVertical />
                                        </IconButton>
                                    }
                                    title={mock.title}>
                                </CardHeader>
                                <Grid container spacing={0} style={styles.grid}>
                                    <Grid item xs={6}>
                                        <Grid container spacing={16}>
                                            <Grid item xs={4}>
                                                <IconButton style={styles.icon} onClick={() => this.handleClick(0, key)}>
                                                    <ChartArc style={{ ...styles.icon, color: mock.selected === 0 ? '#00b0ff' : 'lightGrey'}}/>
                                                </IconButton>
                                            </Grid>
                                            <Grid item xs={4}>
                                                <IconButton style={styles.icon} onClick={() => this.handleClick(1, key)}>
                                                    <ChartBubble style={{ ...styles.icon, color: mock.selected === 1 ? '#00b0ff' : 'lightGrey'}}/>
                                                </IconButton>
                                            </Grid>
                                            <Grid item xs={4}>
                                                <IconButton style={styles.icon} onClick={() => this.handleClick(2, key)}>
                                                    <Gauge style={{ ...styles.icon, color: mock.selected === 2 ? '#00b0ff' : 'lightGrey'}}/>
                                                </IconButton>
                                            </Grid>
                                            <Grid item xs={4}>
                                                <IconButton style={styles.icon} onClick={() => this.handleClick(3, key)}>
                                                    <ChartBar style={{ ...styles.icon, color: mock.selected === 3 ? '#00b0ff' : 'lightGrey'}}/>
                                                </IconButton>
                                            </Grid>
                                            <Grid item xs={4}>
                                                <IconButton style={styles.icon} onClick={() => this.handleClick(4, key)}>
                                                    <ChartLine style={{ ...styles.icon, color: mock.selected === 4 ? '#00b0ff' : 'lightGrey'}}/>
                                                </IconButton>
                                            </Grid>
                                            <Grid item xs={4}>
                                                <IconButton style={styles.icon} onClick={() => this.handleClick(5, key)}>
                                                    <ChartGantt style={{ ...styles.icon, color: mock.selected === 5 ? '#00b0ff' : 'lightGrey'}}/>
                                                </IconButton>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <TextField
                                            style={{width: '50%'}}
                                            label={"TitleField"}
                                            value={"title"}
                                            margin="normal"
                                        />
                                        <br />
                                        <TextField
                                            style={{width: '50%'}}
                                            label={"ValueField"}
                                            value={"value"}
                                            margin="normal"
                                        />
                                        <br />
                                        <TextField
                                            style={{width: '50%'}}
                                            label={"Title"}
                                            value={"Sales"}
                                            margin="normal"
                                        />
                                        <br />
                                    </Grid>
                                </Grid>
                            </Card>
                        )
                    })}
                </div>
            </MainLayout>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}

function mapStateToProps({ }) {
    return { };
}

export default connect(mapStateToProps, mapDispatchToProps)(Widgets);