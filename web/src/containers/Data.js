import React, { Component } from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import {} from "../actions";
import {Routes} from "../utilities/Enums";
import Redirect from "react-router-dom/es/Redirect";
import MainLayout from "../components/MainLayout";
import Grid from "@material-ui/core/es/Grid/Grid";
import DataCard from "../components/Data/DataCard";

const salesData = [
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 1757,
        "Date": "7/29/2018",
        "Time": "6:02 PM",
        "Region": "GA",
        "Static_Amount": 3000,
        "Diff": -1243,
        "Saving_Delta": -1243,
        "Sale_Delta": 0,
        "Dynamic_Cap": 3200,
        "Static_Approved": "TRUE",
        "DynApproved": "TRUE",
        "Weekday": "Sunday",
        "SalesCycleFactor": 0.13
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 189,
        "Date": "7/14/2018",
        "Time": "10:52 AM",
        "Region": "GA",
        "Static_Amount": 3000,
        "Diff": -2811,
        "Saving_Delta": -2811,
        "Sale_Delta": 0,
        "Dynamic_Cap": 3200,
        "Static_Approved": "TRUE",
        "DynApproved": "TRUE",
        "Weekday": "Saturday",
        "SalesCycleFactor": 0.26
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 1872,
        "Date": "7/20/2018",
        "Time": "9:02 AM",
        "Region": "TN",
        "Static_Amount": 3000,
        "Diff": -1128,
        "Saving_Delta": -1128,
        "Sale_Delta": 0,
        "Dynamic_Cap": 3200,
        "Static_Approved": "TRUE",
        "DynApproved": "TRUE",
        "Weekday": "Friday",
        "SalesCycleFactor": 0.14
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 797,
        "Date": "7/19/2018",
        "Time": "8:41 AM",
        "Region": "GA",
        "Static_Amount": 3000,
        "Diff": -2203,
        "Saving_Delta": -2203,
        "Sale_Delta": 0,
        "Dynamic_Cap": 3200,
        "Static_Approved": "TRUE",
        "DynApproved": "TRUE",
        "Weekday": "Thursday",
        "SalesCycleFactor": 0.12
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 8503,
        "Date": "7/28/2018",
        "Time": "1:50 PM",
        "Region": "TN",
        "Static_Amount": 3000,
        "Diff": 5503,
        "Saving_Delta": 0,
        "Sale_Delta": 5503,
        "Dynamic_Cap": 3200,
        "Static_Approved": "FALSE",
        "DynApproved": "FALSE",
        "Weekday": "Saturday",
        "SalesCycleFactor": 0.26
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 2382,
        "Date": "7/7/2018",
        "Time": "12:36 PM",
        "Region": "NC",
        "Static_Amount": 3000,
        "Diff": -618,
        "Saving_Delta": -618,
        "Sale_Delta": 0,
        "Dynamic_Cap": 3200,
        "Static_Approved": "TRUE",
        "DynApproved": "TRUE",
        "Weekday": "Saturday",
        "SalesCycleFactor": 0.26
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 4800,
        "Date": "7/14/2018",
        "Time": "5:06 PM",
        "Region": "MS",
        "Static_Amount": 3000,
        "Diff": 1800,
        "Saving_Delta": 0,
        "Sale_Delta": 1800,
        "Dynamic_Cap": 3200,
        "Static_Approved": "FALSE",
        "DynApproved": "FALSE",
        "Weekday": "Saturday",
        "SalesCycleFactor": 0.26
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 129,
        "Date": "7/8/2018",
        "Time": "12:45 PM",
        "Region": "SC",
        "Static_Amount": 3000,
        "Diff": -2871,
        "Saving_Delta": -2871,
        "Sale_Delta": 0,
        "Dynamic_Cap": 3200,
        "Static_Approved": "TRUE",
        "DynApproved": "TRUE",
        "Weekday": "Sunday",
        "SalesCycleFactor": 0.13
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 8970,
        "Date": "7/16/2018",
        "Time": "6:49 PM",
        "Region": "NC",
        "Static_Amount": 3000,
        "Diff": 5970,
        "Saving_Delta": 0,
        "Sale_Delta": 5970,
        "Dynamic_Cap": 3200,
        "Static_Approved": "FALSE",
        "DynApproved": "FALSE",
        "Weekday": "Monday",
        "SalesCycleFactor": 0.13
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 507,
        "Date": "7/21/2018",
        "Time": "1:46 PM",
        "Region": "TN",
        "Static_Amount": 3000,
        "Diff": -2493,
        "Saving_Delta": -2493,
        "Sale_Delta": 0,
        "Dynamic_Cap": 3200,
        "Static_Approved": "TRUE",
        "DynApproved": "TRUE",
        "Weekday": "Saturday",
        "SalesCycleFactor": 0.26
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 3270,
        "Date": "7/3/2018",
        "Time": "11:17 AM",
        "Region": "SC",
        "Static_Amount": 3000,
        "Diff": 270,
        "Saving_Delta": 0,
        "Sale_Delta": 270,
        "Dynamic_Cap": 3200,
        "Static_Approved": "FALSE",
        "DynApproved": "FALSE",
        "Weekday": "Tuesday",
        "SalesCycleFactor": 0.11
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 666,
        "Date": "7/31/2018",
        "Time": "4:15 PM",
        "Region": "AL",
        "Static_Amount": 3000,
        "Diff": -2334,
        "Saving_Delta": -2334,
        "Sale_Delta": 0,
        "Dynamic_Cap": 3200,
        "Static_Approved": "TRUE",
        "DynApproved": "TRUE",
        "Weekday": "Tuesday",
        "SalesCycleFactor": 0.11
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 5507,
        "Date": "7/15/2018",
        "Time": "6:03 PM",
        "Region": "GA",
        "Static_Amount": 3000,
        "Diff": 2507,
        "Saving_Delta": 0,
        "Sale_Delta": 2507,
        "Dynamic_Cap": 3200,
        "Static_Approved": "FALSE",
        "DynApproved": "FALSE",
        "Weekday": "Sunday",
        "SalesCycleFactor": 0.13
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 3131,
        "Date": "7/19/2018",
        "Time": "3:29 PM",
        "Region": "GA",
        "Static_Amount": 3000,
        "Diff": 131,
        "Saving_Delta": 0,
        "Sale_Delta": 131,
        "Dynamic_Cap": 3200,
        "Static_Approved": "FALSE",
        "DynApproved": "TRUE",
        "Weekday": "Thursday",
        "SalesCycleFactor": 0.12
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 640,
        "Date": "7/4/2018",
        "Time": "5:06 PM",
        "Region": "TN",
        "Static_Amount": 3000,
        "Diff": -2360,
        "Saving_Delta": -2360,
        "Sale_Delta": 0,
        "Dynamic_Cap": 3200,
        "Static_Approved": "TRUE",
        "DynApproved": "TRUE",
        "Weekday": "Wednesday",
        "SalesCycleFactor": 0.11
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 7276,
        "Date": "7/12/2018",
        "Time": "6:58 PM",
        "Region": "GA",
        "Static_Amount": 3000,
        "Diff": 4276,
        "Saving_Delta": 0,
        "Sale_Delta": 4276,
        "Dynamic_Cap": 3200,
        "Static_Approved": "FALSE",
        "DynApproved": "FALSE",
        "Weekday": "Thursday",
        "SalesCycleFactor": 0.12
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 3852,
        "Date": "7/2/2018",
        "Time": "4:35 PM",
        "Region": "TN",
        "Static_Amount": 3000,
        "Diff": 852,
        "Saving_Delta": 0,
        "Sale_Delta": 852,
        "Dynamic_Cap": 3200,
        "Static_Approved": "FALSE",
        "DynApproved": "FALSE",
        "Weekday": "Monday",
        "SalesCycleFactor": 0.13
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 7568,
        "Date": "7/8/2018",
        "Time": "5:05 PM",
        "Region": "NC",
        "Static_Amount": 3000,
        "Diff": 4568,
        "Saving_Delta": 0,
        "Sale_Delta": 4568,
        "Dynamic_Cap": 3200,
        "Static_Approved": "FALSE",
        "DynApproved": "FALSE",
        "Weekday": "Sunday",
        "SalesCycleFactor": 0.13
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 2101,
        "Date": "7/13/2018",
        "Time": "8:13 AM",
        "Region": "MS",
        "Static_Amount": 3000,
        "Diff": -899,
        "Saving_Delta": -899,
        "Sale_Delta": 0,
        "Dynamic_Cap": 3200,
        "Static_Approved": "TRUE",
        "DynApproved": "TRUE",
        "Weekday": "Friday",
        "SalesCycleFactor": 0.14
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 5721,
        "Date": "7/31/2018",
        "Time": "11:15 AM",
        "Region": "SC",
        "Static_Amount": 3000,
        "Diff": 2721,
        "Saving_Delta": 0,
        "Sale_Delta": 2721,
        "Dynamic_Cap": 3200,
        "Static_Approved": "FALSE",
        "DynApproved": "FALSE",
        "Weekday": "Tuesday",
        "SalesCycleFactor": 0.11
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 6297,
        "Date": "7/24/2018",
        "Time": "11:06 AM",
        "Region": "NC",
        "Static_Amount": 3000,
        "Diff": 3297,
        "Saving_Delta": 0,
        "Sale_Delta": 3297,
        "Dynamic_Cap": 3200,
        "Static_Approved": "FALSE",
        "DynApproved": "FALSE",
        "Weekday": "Tuesday",
        "SalesCycleFactor": 0.11
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 2296,
        "Date": "7/31/2018",
        "Time": "9:58 AM",
        "Region": "TN",
        "Static_Amount": 3000,
        "Diff": -704,
        "Saving_Delta": -704,
        "Sale_Delta": 0,
        "Dynamic_Cap": 3200,
        "Static_Approved": "TRUE",
        "DynApproved": "TRUE",
        "Weekday": "Tuesday",
        "SalesCycleFactor": 0.11
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 2929,
        "Date": "7/21/2018",
        "Time": "1:41 PM",
        "Region": "SC",
        "Static_Amount": 3000,
        "Diff": -71,
        "Saving_Delta": -71,
        "Sale_Delta": 0,
        "Dynamic_Cap": 3200,
        "Static_Approved": "TRUE",
        "DynApproved": "TRUE",
        "Weekday": "Saturday",
        "SalesCycleFactor": 0.26
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 2776,
        "Date": "7/8/2018",
        "Time": "3:03 PM",
        "Region": "AL",
        "Static_Amount": 3000,
        "Diff": -224,
        "Saving_Delta": -224,
        "Sale_Delta": 0,
        "Dynamic_Cap": 3200,
        "Static_Approved": "TRUE",
        "DynApproved": "TRUE",
        "Weekday": "Sunday",
        "SalesCycleFactor": 0.13
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 129,
        "Date": "7/29/2018",
        "Time": "10:06 AM",
        "Region": "GA",
        "Static_Amount": 3000,
        "Diff": -2871,
        "Saving_Delta": -2871,
        "Sale_Delta": 0,
        "Dynamic_Cap": 3200,
        "Static_Approved": "TRUE",
        "DynApproved": "TRUE",
        "Weekday": "Sunday",
        "SalesCycleFactor": 0.13
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 7369,
        "Date": "7/22/2018",
        "Time": "5:28 PM",
        "Region": "GA",
        "Static_Amount": 3000,
        "Diff": 4369,
        "Saving_Delta": 0,
        "Sale_Delta": 4369,
        "Dynamic_Cap": 3200,
        "Static_Approved": "FALSE",
        "DynApproved": "FALSE",
        "Weekday": "Sunday",
        "SalesCycleFactor": 0.13
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 985,
        "Date": "7/21/2018",
        "Time": "4:58 PM",
        "Region": "TN",
        "Static_Amount": 3000,
        "Diff": -2015,
        "Saving_Delta": -2015,
        "Sale_Delta": 0,
        "Dynamic_Cap": 3200,
        "Static_Approved": "TRUE",
        "DynApproved": "TRUE",
        "Weekday": "Saturday",
        "SalesCycleFactor": 0.26
    },
    {
        "OEM": "Ford",
        "Model": "Fusion",
        "Request": 3058,
        "Date": "7/31/2018",
        "Time": "5:06 PM",
        "Region": "GA",
        "Static_Amount": 3000,
        "Diff": 58,
        "Saving_Delta": 0,
        "Sale_Delta": 58,
        "Dynamic_Cap": 3200,
        "Static_Approved": "FALSE",
        "DynApproved": "TRUE",
        "Weekday": "Tuesday",
        "SalesCycleFactor": 0.11
    }
];

class Data extends Component {
    constructor(props) {
        super(props);
    }

    logout = () => {
        this.props.history.push(Routes.LOGIN.path);
    };

    render() {
        let { history } = this.props;
        return (
            <MainLayout logout={this.logout} history={history}>
                <Grid container spacing={8} style={{padding: 13}}>
                    <Grid item xs={12}>
                        <DataCard title={"Sales Data 1"} subtitle={"Quarterly Data Pull - Source 1"} data={salesData}/>
                    </Grid>
                    <Grid item xs={12}>
                        <DataCard title={"Sales Data 2"} subtitle={"Quarterly Data Pull - Source 2"} data={salesData}/>
                    </Grid>
                    <Grid item xs={12}>
                        <DataCard title={"Sales Data 3"} subtitle={"Quarterly Data Pull - Source 3"} data={salesData}/>
                    </Grid>
                </Grid>
            </MainLayout>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}

function mapStateToProps({ }) {
    return { };
}

export default connect(mapStateToProps, mapDispatchToProps)(Data);