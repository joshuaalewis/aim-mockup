import React, { Component } from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import {} from "../actions";
import {Routes} from "../utilities/Enums";
import Redirect from "react-router-dom/es/Redirect";

class Root extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let { } = this.props;
        return (
            <Redirect to={Routes.LOGIN.path} />
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}

function mapStateToProps({ }) {
    return { };
}

export default connect(mapStateToProps, mapDispatchToProps)(Root);