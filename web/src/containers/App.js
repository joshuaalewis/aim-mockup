import React, { Component } from 'react';
import {Switch, Route, withRouter} from "react-router-dom";

import {Routes} from "../utilities";
import {} from "../actions";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

const styles = {
    root: {
        height: '100vh',
        width: '100%',
    }
};

class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let { } = this.props;
        return (
            <div style={styles.root}>
                <Switch>
                    <Route exact path={Routes.ROOT.path} component={Routes.ROOT.container} />
                    <Route path={Routes.LOGIN.path} component={Routes.LOGIN.container} />
                    <Route path={Routes.DASHBOARD.path} component={Routes.DASHBOARD.container} />
                    <Route path={Routes.SOURCES.path} component={Routes.SOURCES.container} />
                    <Route path={Routes.DATA.path} component={Routes.DATA.container} />
                    <Route path={Routes.WIDGETS.path} component={Routes.WIDGETS.container} />
                </Switch>
            </div>

        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}

function mapStateToProps({ }) {
    return { };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));