import React, { Component } from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import {} from "../actions";
import LoginLayout from "../components/Login/LoginLayout";
import LoginCard from "../components/Login/LoginCard";
import {Routes} from "../utilities/Enums";

class Login extends Component {
    constructor(props) {
        super(props);
    }

    login = (username, password) => {
        this.props.history.push(Routes.DASHBOARD.path);
    };

    render() {
        let { } = this.props;
        return (
            <LoginLayout>
                <LoginCard login={this.login}/>
            </LoginLayout>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}

function mapStateToProps({ }) {
    return { };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);