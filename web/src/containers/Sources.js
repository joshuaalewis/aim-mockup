import React, { Component } from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import {} from "../actions";
import {Routes} from "../utilities/Enums";
import Redirect from "react-router-dom/es/Redirect";
import MainLayout from "../components/MainLayout";
import Grid from "@material-ui/core/es/Grid/Grid";
import Card from "@material-ui/core/es/Card/Card";
import Avatar from "@material-ui/core/es/Avatar/Avatar";
import {Check, Database, DotsVertical, Pencil, Plus, TableSearch} from "mdi-material-ui";
import CardHeader from "@material-ui/core/es/CardHeader/CardHeader";
import IconButton from "@material-ui/core/es/IconButton/IconButton";
import CardContent from "@material-ui/core/es/CardContent/CardContent";
import TextField from "@material-ui/core/es/TextField/TextField";

const mockData = [
    {
        name: "MySQL",
        type: "MySQL",
        category: "Database",
        image: require('../assets/mysql.png'),
        connection: "Server=myServerAddress;Port=1234;Database=myDataBase;Uid=myUsername;",
        password: "password",
    },
    {
        name: "MSSQL",
        type: "MSSQL",
        category: "Database",
        image: require('../assets/mssql.png'),
        connection: "Server=myServerAddress;Port=1234;Database=myDataBase;Uid=myUsername;",
        password: "password",
    },
    {
        name: "Postgres",
        type: "Postgres",
        category: "Database",
        image: require('../assets/postgres.png'),
        connection: "Server=myServerAddress;Port=1234;Database=myDataBase;Uid=myUsername;",
        password: "password",
    },
    {
        name: "Mongo",
        type: "Mongo",
        category: "Database",
        image: require('../assets/mongo.png'),
        connection: "mongodb://[username:password@]host1[:port1][,host2[:port2],...[,hostN[:portN]]][/[database][?options]]",
        password: "password",
    },
    {
        name: "Excel",
        type: "Excel",
        category: "Document",
        image: require('../assets/excel.png'),
        connection: "Excel File=C:\\myExcelFile.xlsx;Cache Location=C:\\cache.db;Auto Cache=true;",
        password: "password",
    },
];

class Sources extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mocks: mockData,
            editing: null
        };
    }

    logout = () => {
        this.props.history.push(Routes.LOGIN.path);
    };

    handleChange = (prop, index) => event => {
        let mocks = [ ...this.state.mocks ];
        mocks[index][prop] = event.target.value;
        this.setState({mocks: mocks});
    };

    handleEdit = key => {
        if(this.state.editing === key) {
            this.setState({editing: null});
        } else {
            this.setState({editing: key});
        }
    };

    render() {
        let { mocks, editing } = this.state;
        const styles = {
            img: {
                backgroundSize: '80%',
                backgroundRepeat: 'no-repeat',
                // backgroundPosition: 'center 20%',
                paddingLeft: 30,
                margin: '0 auto',
                height: 200,
                minHeight: '80px'
            },
        };

        let { history, } = this.props;

        return (
            <MainLayout logout={this.logout} history={history}>
                <Grid container spacing={16} style={{padding: 13}}>
                    {mocks.map((mock, key) => {
                        return (
                            <Grid item xs={12} md={6}>
                                <Card >
                                    <CardHeader
                                        avatar={
                                            <Avatar aria-label="Source" style={{backgroundColor: '#00b0ff'}}>
                                                <Database />
                                            </Avatar>
                                        }
                                        action={
                                            <IconButton onClick={() => this.handleEdit(key)}>
                                                {(editing !== key) && <Pencil />}
                                                {(editing === key) && <Check />}
                                            </IconButton>
                                        }
                                        title={mock.name}
                                        subheader={mock.category}>
                                    </CardHeader>
                                    <CardContent style={{display: 'flex', flexDirection: 'row'}}>
                                        <div style={{flex: '1 auto'}}>
                                            <div style={ { ...styles.img, backgroundImage: `url(${mock.image})` }}/>
                                        </div>
                                        <div style={{flex: '2 auto'}}>
                                            <TextField
                                                style={{width: '50%'}}
                                                label={"Name"}
                                                value={mock.name}
                                                onChange={this.handleChange('name', key)}
                                                margin="normal"
                                                disabled={editing !== key}
                                            />
                                            <br />
                                            <TextField
                                                style={{width: '50%'}}
                                                label={"Type"}
                                                value={mock.type}
                                                onChange={this.handleChange('type', key)}
                                                margin="normal"
                                                disabled={editing !== key}
                                            />
                                            <br />
                                            <TextField
                                                style={{width: '90%'}}
                                                label={"Connection"}
                                                value={mock.connection}
                                                onChange={this.handleChange('connection', key)}
                                                margin="normal"
                                                disabled={editing !== key}
                                            />
                                            <br />
                                            <TextField
                                                style={{width: '50%'}}
                                                label={"Password"}
                                                value={mock.password}
                                                onChange={this.handleChange('password', key)}
                                                margin="normal"
                                                disabled={editing !== key}
                                                type={"password"}
                                            />
                                        </div>
                                    </CardContent>
                                </Card>
                            </Grid>
                        )
                    })}
                    <Grid item xs={12} md={6} style={{textAlign: 'center'}}>
                        <IconButton style={{width: 200, height: 200, margin: '80px auto'}}>
                            <Plus style={{width: 200, height: 200, color: '#cbcbcb'}}/>
                        </IconButton>
                    </Grid>
                </Grid>
            </MainLayout>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}

function mapStateToProps({ }) {
    return { };
}

export default connect(mapStateToProps, mapDispatchToProps)(Sources);