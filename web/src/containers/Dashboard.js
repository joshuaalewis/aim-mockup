import React, { Component } from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import {} from "../actions";
import {Routes} from "../utilities/Enums";
import MainLayout from "../components/MainLayout";
import Grid from "@material-ui/core/es/Grid/Grid";
import WidgetCard from "../components/Dashboard/WidgetCard";

const mocks = [
    {
        title: "Sales Distribution",
        subtitle: "Quarter - By Day",
        data: [
            {
                title: "Saturday",
                value: 435
            },
            {
                title: "Sunday",
                value: 340
            },
            {
                title: "Friday",
                value: 123
            },
            {
                title: "Wednesday",
                value: 80
            },
            {
                title: "Thursday",
                value: 45
            },
        ],
        largeDisplay: false,
        chartOptions: {
            type: "pie",
            theme: "light",
            titleField: "title",
            valueField: "value",
            radius: "32%",
            innerRadius: "50%",
            export: {
                enabled: true
            }
        }
    },
    {
        title: "Sales Growth",
        subtitle: "Quarterly",
        data: [
            {
                quarter: "Q1",
                sales: 67
            },
            {
                quarter: "Q2",
                sales: 45
            },
            {
                quarter: "Q3",
                sales: 25
            },
            {
                quarter: "Q4",
                sales: 85
            },
        ],
        largeDisplay: false,
        chartOptions: {
            type: "serial",
            theme: "light",
            categoryField: "quarter",
            categoryAxis: {
                gridThickness: 0
            },
            valueAxes: [
                {
                    id: "ValueAxis-1",
                    gridThickness: 0,
                    gridAlpha: 0.0,
                }
            ],
            chartCursor: {
                enabled: true
            },
            titles: [
                {
                    bold: false,
                    id: "Title-1",
                    size: 15,
                    text: "Sales Over Time"
                }
            ],
            graphs: [ {
                fillAlphas: 1.0,
                lineAlpha: 0.0,
                type: "column",
                valueAxis: "ValueAxis-1",
                title: "Sales",
                valueField: "sales"
            }],
            export: {
                enabled: true
            }
        }
    },
    {
        title: "Diff Plot",
        subtitle: "Quarterly",
        data: [
            {
                "date": "2015-01-01",
                "ay": 6.5,
                "by": 2.2,
                "aValue": 15,
                "bValue": 10
            }, {
                "date": "2015-01-02",
                "ay": 12.3,
                "by": 4.9,
                "aValue": 8,
                "bValue": 3
            }, {
                "date": "2015-01-03",
                "ay": 12.3,
                "by": 5.1,
                "aValue": 16,
                "bValue": 4
            }, {
                "date": "2015-01-04",
                "ay": 2.8,
                "by": 13.3,
                "aValue": 9,
                "bValue": 13
            }, {
                "date": "2015-01-05",
                "ay": 3.5,
                "by": 6.1,
                "aValue": 5,
                "bValue": 2
            }, {
                "date": "2015-01-06",
                "ay": 5.1,
                "by": 8.3,
                "aValue": 10,
                "bValue": 17
            }, {
                "date": "2015-01-07",
                "ay": 6.7,
                "by": 10.5,
                "aValue": 3,
                "bValue": 10
            }, {
                "date": "2015-01-08",
                "ay": 8,
                "by": 12.3,
                "aValue": 5,
                "bValue": 13
            }, {
                "date": "2015-01-09",
                "ay": 8.9,
                "by": 4.5,
                "aValue": 8,
                "bValue": 11
            }, {
                "date": "2015-01-10",
                "ay": 9.7,
                "by": 15,
                "aValue": 15,
                "bValue": 10
            }, {
                "date": "2015-01-11",
                "ay": 10.4,
                "by": 10.8,
                "aValue": 1,
                "bValue": 11
            }, {
                "date": "2015-01-12",
                "ay": 1.7,
                "by": 19,
                "aValue": 12,
                "bValue": 3
            }
        ],
        largeDisplay: false,
        chartOptions: {
            type: "xy",
            theme: "none",
            marginRight: 80,
            dataDateFormat: "YYYY-MM-DD",
            startDuration: 1.5,
            trendLines: [],
            balloon: {
                adjustBorderColor: false,
                shadowAlpha: 0,
                fixedPosition:true
            },
            graphs: [{
                balloonText: "<div style='margin:5px;'><b>[[x]]</b><br>y:<b>[[y]]</b><br>value:<b>[[value]]</b></div>",
                bullet: "diamond",
                id: "AmGraph-1",
                lineAlpha: 0,
                lineColor: "#0077de",
                fillAlphas: 0,
                valueField: "aValue",
                xField: "date",
                yField: "ay"
            }, {
                balloonText: "<div style='margin:5px;'><b>[[x]]</b><br>y:<b>[[y]]</b><br>value:<b>[[value]]</b></div>",
                bullet: "round",
                id: "AmGraph-2",
                lineAlpha: 0,
                lineColor: "#009688",
                fillAlphas: 0,
                valueField: "bValue",
                xField: "date",
                yField: "by"
            }],
            valueAxes: [{
                id: "ValueAxis-1",
                axisAlpha: 0
            }, {
                id: "ValueAxis-2",
                axisAlpha: 0,
                position: "bottom",
                type: "date",
                minimumDate: new Date(2014, 11, 31),
                maximumDate: new Date(2015, 0, 13)
            }],
            export: {
                enabled: true
            },
            chartCursor:{
                pan:true,
                cursorAlpha:0,
                valueLineAlpha:0
            }
        }
    },
    {
        title: "Sales Goals",
        subtitle: "Group Target Sales - Quarter",
        data: [

        ],
        largeDisplay: false,
        chartOptions: {
            type: "gauge",
            axes: [{
                "topTextFontSize": 20,
                "topTextYOffset": 70,
                "axisColor": "#31d6ea",
                "axisThickness": 1,
                "endValue": 100,
                "gridInside": true,
                "inside": true,
                "valueInterval": 10,
                "tickColor": "#67b7dc",
                "startAngle": -90,
                "endAngle": 90,
                "unit": "%",
                "bandOutlineAlpha": 0,
                "bands": [{
                    "color": "#0080ff",
                    "endValue": 100,
                    "gradientRatio": [0.5, 0, -0.5],
                    "startValue": 0
                }, {
                    "color": "#3cd3a3",
                    "endValue": 0,
                    "gradientRatio": [0.5, 0, -0.5],
                    "startValue": 0
                }]
            }],
            arrows: [{
                "alpha": 1,
                "nailRadius": 0,
                "value": 60,
            }]
        }
    },
    {
        title: "Sales",
        subtitle: "Quarterly",
        data: [
            {
                "date": "2012-01-01",
                "value": 8
            }, {
                "date": "2012-01-02",
                "color":"#CC0000",
                "value": 10
            }, {
                "date": "2012-01-03",
                "value": 12
            }, {
                "date": "2012-01-04",
                "value": 14
            }, {
                "date": "2012-01-05",
                "value": 11
            }, {
                "date": "2012-01-06",
                "value": 6
            }, {
                "date": "2012-01-07",
                "value": 7
            }, {
                "date": "2012-01-08",
                "value": 9
            }, {
                "date": "2012-01-09",
                "value": 13
            }, {
                "date": "2012-01-10",
                "value": 15
            }, {
                "date": "2012-01-11",
                "color":"#CC0000",
                "value": 19
            }, {
                "date": "2012-01-12",
                "value": 21
            }, {
                "date": "2012-01-13",
                "value": 22
            }, {
                "date": "2012-01-14",
                "value": 20
            }, {
                "date": "2012-01-15",
                "value": 18
            }, {
                "date": "2012-01-16",
                "value": 14
            }, {
                "date": "2012-01-17",
                "color":"#CC0000",
                "value": 16
            }, {
                "date": "2012-01-18",
                "value": 18
            }, {
                "date": "2012-01-19",
                "value": 17
            }, {
                "date": "2012-01-20",
                "value": 15
            }, {
                "date": "2012-01-21",
                "value": 12
            }, {
                "date": "2012-01-22",
                "color":"#CC0000",
                "value": 10
            }, {
                "date": "2012-01-23",
                "value": 8
            }
        ],
        largeDisplay: true,
        chartOptions: {
            "type": "serial",
            "theme": "none",
            "marginRight":80,
            "autoMarginOffset":20,
            "dataDateFormat": "YYYY-MM-DD HH:NN",
            "dataProvider": [],
            "valueAxes": [{
                "axisAlpha": 0,
                "guides": [{
                    "fillAlpha": 0.1,
                    "fillColor": "#888888",
                    "lineAlpha": 0,
                    "toValue": 16,
                    "value": 10
                }],
                "position": "left",
                "tickLength": 0
            }],
            "graphs": [{
                "balloonText": "[[category]]<br><b><span style='font-size:14px;'>value:[[value]]</span></b>",
                "bullet": "round",
                "dashLength": 3,
                "colorField":"color",
                "valueField": "value"
            }],
            "trendLines": [{
                "finalDate": "2012-01-11 12",
                "finalValue": 19,
                "initialDate": "2012-01-02 12",
                "initialValue": 10,
                "lineColor": "#CC0000"
            }, {
                "finalDate": "2012-01-22 12",
                "finalValue": 10,
                "initialDate": "2012-01-17 12",
                "initialValue": 16,
                "lineColor": "#CC0000"
            }],
            "chartScrollbar": {
                "scrollbarHeight":2,
                "offset":-1,
                "backgroundAlpha":0.1,
                "backgroundColor":"#888888",
                "selectedBackgroundColor":"#67b7dc",
                "selectedBackgroundAlpha":1
            },
            "chartCursor": {
                "fullWidth":true,
                "valueLineEabled":true,
                "valueLineBalloonEnabled":true,
                "valueLineAlpha":0.5,
                "cursorAlpha":0
            },
            "categoryField": "date",
            "categoryAxis": {
                "parseDates": true,
                "axisAlpha": 0,
                "gridAlpha": 0.1,
                "minorGridAlpha": 0.1,
                "minorGridEnabled": true
            },
            "export": {
                "enabled": true
            }
        }

    },
];

class Dashboard extends Component {
    constructor(props) {
        super(props);
    }

    logout = () => {
        this.props.history.push(Routes.LOGIN.path);
    };

    render() {
        let { history } = this.props;
        return (
            <MainLayout logout={this.logout} history={history}>
                <Grid container spacing={8}>
                    {mocks.map((mock, key) => {
                        return (
                            <Grid item xs={12} sm={mock.largeDisplay ? 12 : 6} md={mock.largeDisplay ? 8 : 4} key={key}>
                                <WidgetCard title={mock.title} subtitle={mock.subtitle} data={mock.data} chartOptions={mock.chartOptions}/>
                            </Grid>
                        )
                    })}
                </Grid>
            </MainLayout>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}

function mapStateToProps({ }) {
    return { };
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);